(defpackage #:crash.cd
  (:use #:cl #:crash))

(in-package #:crash.cd)

(asdf:defsystem #:crash.cd
  :description "Cd plugin for crash"
  :author "Florian Margaine <florian@margaine.com>"
  :license "MIT License"
  :serial t
  :depends-on (:crash)
  :components ((:file "cd")))
